# System Recovery

## Rolling Back `Pop_OS!`

- press `F12` to access `Boot Menu` and boot from live usb
- connect to Wi-Fi
- run the commands bellow

```bash
sudo cryptsetup luksOpen /dev/sda3 popos
sudo mount /dev/data/root /mnt
sudo apt install -y timeshift
```

- use `Timeshift` to roll back to earlier snapshot

## Chroot Into Arch Linux - Dinner Room PC

```bash
cryptsetup open --type luks /dev/sda3 archlinux
mount -t btrfs -o subvol=root /dev/mapper/archlinux /mnt
mount -t btrfs -o subvol=home /dev/mapper/archlinux /mnt/home
mount -t btrfs -o subvol=snapshots /dev/mapper/archlinux /mnt/snapshots
mount /dev/sda2 /mnt/boot 
mount /dev/sda1 /mnt/boot/efi
arch-chroot /mnt
```
