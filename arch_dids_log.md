# Arch Problems/Solutions Log

- [Arch Problems/Solutions Log](#arch-problemssolutions-log)
  - [12.03.2021](#12032021)
    - [Problem](#problem)
    - [Solution](#solution)
  - [13.03.2021](#13032021)
  - [14.03.2021](#14032021)
    - [Steps Taken](#steps-taken)
    - [Results](#results)
    - [Solution to Grub Theme](#solution-to-grub-theme)
  - [16.03.2021](#16032021)
  - [18.03.2021](#18032021)
  - [29.03.2021](#29032021)
    - [Issue](#issue)
    - [Solution - Mount on Boo](#solution---mount-on-boo)
  - [06.04.2021](#06042021)
  - [08.04.2021](#08042021)
  - [Install Steam](#install-steam)
  - [Brother DS-640 Setup](#brother-ds-640-setup)
    - [References](#references)
  - [Install KVM, QEMU and Virt Manager on Arch Linux](#install-kvm-qemu-and-virt-manager-on-arch-linux)
    - [References](#references-1)
  - [TODOs](#todos)

## 12.03.2021

### Problem

- failed to boot after the update
- kernel  version mismatch between `uname -a` and `pacman -Q linux`
  - `5.10.23` vs `5.11.6`
- partitions in `/etc/fstab` were mounted wrongly

```bash
# Static information about the filesystems.
# See fstab(5) for details.

# <file system> <dir> <type> <options> <dump> <pass>
# /dev/mapper/archlinux LABEL=root
UUID=fd870149-d32d-4e58-8be0-254da69beb7c       /               btrfs           rw,relatime,ssd,space_cache,subvolid=256,subvol=/root,subvol=root   0 0

# /dev/sda1 LABEL=EFI\134x20System
UUID=0993-680F          /boot           vfat            rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro        0 2

# /dev/sda2 LABEL=boot
UUID=1d5cd7ca-4c97-4003-80ec-c990bc5375f2       /boot/efi       ext4            rw,relatime     0 2

# /dev/mapper/archlinux LABEL=root
UUID=fd870149-d32d-4e58-8be0-254da69beb7c       /home           btrfs           rw,relatime,ssd,space_cache,subvolid=257,subvol=/home,subvol=home   0 0

# /dev/mapper/archlinux LABEL=root
UUID=fd870149-d32d-4e58-8be0-254da69beb7c       /snapshots      btrfs           rw,relatime,ssd,space_cache,subvolid=258,subvol=/snapshots,subvol=snapshots  0 0

/swap/swapfile          none            swap            defaults        0 0
```

### Solution

- chroot to the system from live media and removed the old `/etc/fstab` after creating a copy
- the solution was found in: [Linux kernel mismatch on `/boot` and pacman -Q linux](https://bbs.archlinux.org/viewtopic.php?id=243417) - # 9

```bash
# UUID=fd870149-d32d-4e58-8be0-254da69beb7c LABEL=root
/dev/mapper/archlinux   /               btrfs           rw,relatime,ssd,space_cache,subvolid=256,subvol=/root,subvol=root       0 0

# UUID=1d5cd7ca-4c97-4003-80ec-c990bc5375f2 LABEL=boot
/dev/sda2               /boot           ext4            rw,relatime     0 2

# UUID=0993-680F LABEL=EFI\134x20System
/dev/sda1               /boot/efi       vfat            rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro      0 2

# UUID=fd870149-d32d-4e58-8be0-254da69beb7c LABEL=root
/dev/mapper/archlinux   /home           btrfs           rw,relatime,ssd,space_cache,subvolid=257,subvol=/home,subvol=home       0 0

# UUID=fd870149-d32d-4e58-8be0-254da69beb7c LABEL=root
/dev/mapper/archlinux   /snapshots      btrfs           rw,relatime,ssd,space_cache,subvolid=258,subvol=/snapshots,subvol=snapshots     0 0

# SWAPFILE
/swap/swapfile          none            swap            defaults        0 0
```

## 13.03.2021

- switch to `linux-lts` kernel - `5.10.23`
- enable `REISUB` - "**R**eboot **E**ven **I**f **S**ystem **U**tterly **B**roken" 
  - Alt+PrtSc
  - lightly tap these keys *waiting between 1 second (fast, new machines) and 6 seconds (older or resource-starved machines¹) in-between keypresses* : R E I S U B
  - https://wiki.archlinux.org/index.php/Keyboard_shortcuts#Kernel_(SysRq)
  - https://wiki.archlinux.org/index.php/Sysctl#Configuration
  - https://forum.manjaro.org/t/howto-reboot-turn-off-your-frozen-computer-reisub-reisuo/3855

```bash
echo kernel.sysrq=1 | sudo tee --append /etc/sysctl.d/99-sysctl.conf
sudo update-grub # command not found
reboot
```

- switch KDE/GTK theme to: [Adapta Nokto](https://store.kde.org/p/1191695/)
- [adjustments to `/etc/makepkg.conf`](https://wiki.archlinux.org/index.php/makepkg)
  - utilize all cores on ZSTD compression: `COMPRESSZST=(zstd -c -z -q - --threads=0)`
  - use all cores on build times: `MAKEFLAGS="-j$(nproc)"`
  - switch build directory: `BUILDDIR=/home/ystanev/aur_builds`
- [adjustments to `/etc/pacman.conf`](https://wiki.archlinux.org/index.php/pacman)
  - [uncomment `VerbosePkgLists`](https://wiki.archlinux.org/index.php/pacman#Comparing_versions_before_updating) - displays name, version and size of target packages formatted as a table for upgrade, sync and remove operations
  - [uncomment `Color`](https://wiki.archlinux.org/index.php/Color_output_in_console#pacman) - automatically enable colors only when `pacman`’s output is on a `tty`

## 14.03.2021

### Steps Taken

- tried to theme Grub menu - `WIP`
  - downloaded the [grub theme](https://www.gnome-look.org/p/1009236/)
  - copied the extracted archive to theme folder: `sudo cp -r Vimix-1080p/Vimix /usr/share/grub/themes`
  - set Grub theme as [per Arch Wiki](https://wiki.archlinux.org/index.php/GRUB/Tips_and_tricks#Theme)
    - `GRUB_THEME="/usr/share/grub/themes/Vimix/theme.txt"`
  - [re-generated main config file](https://wiki.archlinux.org/index.php/GRUB#Generate_the_main_configuration_file): `grub-mkconfig -o /boot/grub/grub.cfg`

### Results

- got a bunch of errors about files not found

### Solution to Grub Theme

- found the solution on [Reddit](https://www.reddit.com/r/archlinux/comments/cmtnld/error_on_boot_with_nondefault_grub_theme/)

```bash
sudo mv /usr/share/grub/themes/Vimix /boot/grub/themes
sudo nano /etc/default/grub # GRUB_THEME="/boot/grub/themes/Vimix/theme.txt"
grub-mkconfig -o /boot/grub/grub.cfg
```

## 16.03.2021

- decrease number of snapshots for `/` and `/home`:
  - `NUMBER_LIMIT="5"`
  - `TIMELINE_MIN_AGE="1800"`
  - `TIMELINE_LIMIT_HOURLY="3"`
  - `TIMELINE_LIMIT_DAILY="2"`
  - `TIMELINE_LIMIT_WEEKLY="1"`
  - `TIMELINE_LIMIT_MONTHLY="0"`
  - `TIMELINE_LIMIT_YEARLY="0"`

- [connect bluetooth to `WH-1000XM3` headphones](https://wiki.archlinux.org/index.php/bluetooth_headset):

```bash
sudo pacman -S bluez # https://wiki.archlinux.org/index.php/Bluetooth#Installation
sudo pacman -S bluez-utils
sudo systemctl enable bluetooth.service --now
sudo pacman -S pulseaudio-bluetooth
sudo pacman -S pulseaudio-alsa
sudo systemctl restart bluetooth.service 
pulseaudio -k # if you are getting a connection error org.bluez.Error.Failed retry by killing existing PulseAudio daemon first
bluetoothctl # prompt will switch to "[bluetooth]# "

[bluetooth]# power on
[bluetooth]# agent on
[bluetooth]# default-agent
[bluetooth]# scan on
  [NEW] Device 94:DB:56:54:78:A8 LE_WH-1000XM3
[bluetooth]# pair 94:DB:56:54:78:A8
[bluetooth]# trust 94:DB:56:54:78:A8
[bluetooth]# connect 94:DB:56:54:78:A8
  [CHG] Device 94:DB:56:54:78:A8 Connected: yes
  [CHG] Device 94:DB:56:54:78:A8 Name: WH-1000XM3
  [CHG] Device 94:DB:56:54:78:A8 Alias: WH-1000XM3
  Connection successful
[WH-1000XM3]# exit
```

- setup HP Envy 5640 Printer - `WIP`
  - https://wiki.archlinux.org/index.php/CUPS
  - https://wiki.archlinux.org/index.php/CUPS/Printer-specific_problems#HP
  - http://localhost:631/
  - the printer print the specified docs, but the pages are blank
    - could be ink issue
      - no black inks
      - half of tri-color ink
    - Nikolai mentioned **the printer just stopped working** a while back

```bash
sudo pacman -S cups
sudo systemctl enable cups.service --now
sudo pacman -S hplip
hp-setup -i 192.168.0.135
```

- look into solving the following error: `ssh_askpass: exec(/usr/lib/ssh/ssh-askpass): No such file or directory` - ***not crucial***
  - https://stackoverflow.com/questions/17846529/could-not-open-a-connection-to-your-authentication-agent
  - https://wiki.archlinux.org/index.php/SSH_keys#ssh-agent - **PREFERRED OPTION**
  - https://code.visualstudio.com/docs/remote/troubleshooting#_setting-up-the-ssh-agent

```bash
➜  ~ ssh-agent           
SSH_AUTH_SOCK=/tmp/ssh-XXXXXXy0xuwj/agent.129290; export SSH_AUTH_SOCK;
SSH_AGENT_PID=129292; export SSH_AGENT_PID;
echo Agent pid 129292;
➜  ~ eval $(ssh-agent)
Agent pid 130768
➜  ~ ssh-add .ssh/id_rsa
Enter passphrase for .ssh/id_rsa: 
Identity added: .ssh/id_rsa (ystanev@yahoo.ca)
```

## 18.03.2021

- got the [asian characters to display properly in Firefox](https://unix.stackexchange.com/a/356090)

```bash
sudo pacman -S noto-fonts-cjk noto-fonts-emoji noto-fonts-extra
```

## 29.03.2021

- add support for `NTFS`: `sudo pacman -S ntfs-3g`
- install borg backup and Vorta GUI: `yay -S borg vorta`
- setup borg backup to run everyday at 11 pm to external USB (Vorta GUI)
  - password same as the Linux Mint PC

![vorta1](resources/vorta1.jpg)
![vorta2](resources/vorta2.jpg)
![vorta3](resources/vorta3.jpg)
![vorta4](resources/vorta4.jpg)
![vorta5](resources/vorta5.jpg)

- `borgmatic` - [simple, configuration-driven backup software for servers and workstations](https://torsion.org/borgmatic/)

- reference links
  - https://borgbackup.readthedocs.io/en/stable/deployment/automated-local.html
  - https://borgbackup.readthedocs.io/en/stable/usage/list.html
  - https://borgbackup.readthedocs.io/en/stable/usage/info.html
  - https://borgbackup.readthedocs.io/en/stable/usage/init.html
  - https://borgbackup.readthedocs.io/en/stable/usage/create.html
  - https://vorta.borgbase.com/usage/restore/
  - https://docs.borgbase.com/restore/borg/
  - https://medium.com/swlh/backing-up-with-borg-c6f13d74dd6

### Issue

- scheduled backup didn't run after reboot due to usb not being mounted on boot

### Solution - Mount on Boo

- https://confluence.jaytaala.com/display/TKB/Mount+drive+in+linux+and+set+auto-mount+at+boot
- https://askubuntu.com/questions/113733/how-to-mount-a-ntfs-partition-in-etc-fstab

```bash
# UUID=fd870149-d32d-4e58-8be0-254da69beb7c LABEL=root
/dev/mapper/archlinux   /               btrfs           rw,relatime,ssd,space_cache,subvolid=256,subvol=/root,subvol=root       0 0

# UUID=1d5cd7ca-4c97-4003-80ec-c990bc5375f2 LABEL=boot
/dev/sda2               /boot           ext4            rw,relatime     0 2

# UUID=0993-680F LABEL=EFI\134x20System
/dev/sda1               /boot/efi       vfat            rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro      0 2

# UUID=fd870149-d32d-4e58-8be0-254da69beb7c LABEL=root
/dev/mapper/archlinux   /home           btrfs           rw,relatime,ssd,space_cache,subvolid=257,subvol=/home,subvol=home       0 0

# UUID=fd870149-d32d-4e58-8be0-254da69beb7c LABEL=root
/dev/mapper/archlinux   /snapshots      btrfs           rw,relatime,ssd,space_cache,subvolid=258,subvol=/snapshots,subvol=snapshots     0 0

# SWAPFILE
/swap/swapfile          none            swap            defaults        0 0

# BORG BACKUP USB UUID=0A7983F224124082
UUID=0A7983F224124082               /home/ystanev/borg_backup_repo/         ntfs-3g         defaults        0 0
```

- ~~***THE ABOVE SOLUTION DIDN'T WORK, THERE WAS A TIMEOUT WHEN TRYING TO MOUNT `/dev/sdc1`, FIXED BY RESTORING `/etc/fstab` FROM BACKUP***~~

- POSSIBLE FIX:
  - switch the `/dev/sdc1` in `/etc/fstab.bad` to `UUID` as proposed in the links

## 06.04.2021

- [install virtualbox](https://wiki.archlinux.org/index.php/VirtualBox#Installation_steps_for_Arch_Linux_hosts) on `linux-lts` kernel

```bash
sudo pacman -S virtualbox virtualbox-host-dkms linux-lts-headers virtualbox-guest-iso # for the linux kernel, choose virtualbox-host-modules-arch
yay -S virtualbox-ext-oracle # AUR package
sudo modprobe vboxdrv # might not be needed
reboot
```

## 08.04.2021

- install [nohang-git](https://aur.archlinux.org/packages/nohang-git/)

```bash
yay -S nohang-git
sudo systemctl enable --now nohang-desktop.service
```

- install [ananicy-git](https://aur.archlinux.org/packages/ananicy-git)

```bash
yay -S ananicy-git
sudo systemctl enable ananicy
sudo systemctl start ananicy
```

## [Install Steam](https://wiki.archlinux.org/title/steam)

- Uncomment the `[multilib]` section in `/etc/pacman.conf`

  ```bash
  [multilib]
  Include = /etc/pacman.d/mirrorlist
  ```

- update system: `sudo pacman -Syu`
- install `steam`: `sudo pacman -S steam`
- install `lib32-fontconfig`, `ttf-liberation` and `wqy-zenhei`(for Asian characters), then restart Steam
- you can enable Proton in the `Steam Client in Steam > Settings > Steam Play`
- to force enable Proton, right click on the game, `Properties > General > Force the use of a specific Steam Play compatibility tool`

## [Brother DS-640 Setup](https://www.brother.ca/en/p/DS640)

- install [SANE](https://wiki.archlinux.org/title/SANE#Installation) and driver from AUR

```bash
sudo pacman -S sane-airscan ipp-usb
yay -S brscan5
sudo systemctl enable ipp-usb.service --now
```

- modily [udev](https://wiki.archlinux.org/title/udev#About_udev_rules) rules

```bash
sudo nano /usr/lib/udev/rules.d/65-sane.rules
# append at the end: ATTR{idProduct}=="046a", MODE="0664", GROUP="lp", ENV{libsane_matched}="yes"
sudo nano sudo nano /etc/udev/rules.d/49-sane-missing-scanner.rules
# append at the end: ATTRS{idVendor}=="04f9", ATTRS{idProduct}=="0468", MODE="0664", GROUP="lp", ENV{libsane_matched}="yes"
```

- unplug/replug scanner for `udev` rules to detect it
- create a test scan (work only with `sudo`)

```bash
cd Desktop
scanimage --format=png --output-file test.png --progress
```

- install a frontend

```bash
sudo pacman -S simple-scan
```

### References

- check permissions
  - https://bbs.archlinux.org/viewtopic.php?id=192103
  - https://wiki.archlinux.org/title/SANE#Permission_problem

## [Install KVM, QEMU and Virt Manager on Arch Linux](https://computingforgeeks.com/install-kvm-qemu-virt-manager-arch-manjar/)

```bash
sudo pacman -S qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat ebtables iptables libguestfs
sudo systemctl enable libvirtd.service --now
# Enable normal user account to use KVM
sudo nano /etc/libvirt/libvirtd.conf
# Set the UNIX domain socket group ownership to libvirt, (around line 85): unix_sock_group = "libvirt"
# Set the UNIX socket permissions for the R/W socket (around line 102): unix_sock_rw_perms = "0770"
sudo usermod -a -G libvirt $(whoami)
newgrp libvirt
sudo systemctl restart libvirtd.service
```

- I had issues with network connectivity in VM
- create a file `br10.xml` with the below content

```xml
<network>
  <name>br10</name>
  <forward mode='nat'>
    <nat>
      <port start='1024' end='65535'/>
    </nat>
  </forward>
  <bridge name='br10' stp='on' delay='0'/>
  <ip address='192.168.30.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.30.50' end='192.168.30.200'/>
    </dhcp>
  </ip>
</network>
```

```bash
sudo virsh net-define br10.xml
sudo virsh net-start br10
sudo virsh net-autostart br10
```

### References

- https://www.youtube.com/watch?v=itZf5FpDcV0&t=328s

## TODOs

- ~~[nohang](https://github.com/hakavlad/nohang) package provides a highly configurable daemon for Linux which is able to correctly prevent out of memory (OOM) and keep system responsiveness in low memory conditions~~
- ~~[Ananicy (ANother Auto NICe daemon)](https://github.com/Nefelim4ag/Ananicy) — is a shell daemon created to manage processes' IO and CPU priorities, with community-driven set of rules for popular applications (anyone may add his own rule via github's pull request mechanism)~~
- [auto-cpufreq](https://github.com/AdnanHodzic/auto-cpufreq) - automatic CPU speed & power optimizer for Linux based on active monitoring of laptop's battery state, CPU usage, CPU temperature and system load
- add issue on 18.04.2021
  - `grub-btrfs.path` woundn't look for new images after update
  - misconfiguration, moved `root`/`home` snapshots to their own subvolumes
  - https://bbs.archlinux.org/viewtopic.php?id=265617
- ~~add notes on dual kernel setup and NVIDIA~~
  - ~~https://archlinux.org/packages/extra/x86_64/nvidia-dkms/~~
  - ~~https://bbs.archlinux.org/viewtopic.php?id=232217~~
  - ~~https://wiki.archlinux.org/index.php/NVIDIA#Custom_kernel~~
  - ~~https://wiki.archlinux.org/index.php/Pacman#Hooks~~
- fix ToC
- ssh issue
  - https://www.reddit.com/r/archlinux/comments/lyazre/openssh_update_causes_problems/
