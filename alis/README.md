# [Arch Linux Install Script](https://github.com/picodotdev/alis)

## System installation

Download and boot from the latest [original Arch Linux installation media](https://www.archlinux.org/download/). After boot use the following commands to start the installation.

Follow the [Arch Way](https://wiki.archlinux.org/title/Arch_Linux) of doing things and learn what this script does. This will allow you to know what is happening.

Internet connection is required, with wireless WIFI connection see [Wireless_network_configuration](https://wiki.archlinux.org/title/Wireless_network_configuration#Wi-Fi_Protected_Access) to bring up WIFI connection before start the installation.

Minimum usage

```bash
#                         # Start the system with latest Arch Linux installation media
# loadkeys [keymap]       # Load keyboard keymap, eg. loadkeys es, loadkeys us, loadkeys de
# curl -sL https://raw.githubusercontent.com/picodotdev/alis/master/download.sh | bash     # Download alis scripts
# vim alis.conf           # Edit configuration and change variables values with your preferences (system configuration)
# ./alis.sh               # Start installation
```

Advanced usage

```bash
#                         # Start the system with latest Arch Linux installation media
# loadkeys [keymap]       # Load keyboard keymap, eg. loadkeys es, loadkeys us, loadkeys de
# iwctl --passphrase "[WIFI_KEY]" station [WIFI_INTERFACE] connect "[WIFI_ESSID]"          # (Optional) Connect to WIFI network. _ip link show_ to know WIFI_INTERFACE.
# curl -sL https://raw.githubusercontent.com/picodotdev/alis/master/download.sh | bash     # Download alis scripts
# # curl -sL https://git.io/JeaH6 | bash                                                   # Alternative download URL with URL shortener
# # curl -sL https://raw.githubusercontent.com/picodotdev/alis/master/download.sh | bash -s -- -h [HASH_COMMIT] # Use specific version of the script based on the commit hash
# ./alis-asciinema.sh     # (Optional) Start asciinema video recording
# vim alis.conf           # Edit configuration and change variables values with your preferences (system configuration)
# vim alis-packages.conf  # (Optional) Edit configuration and change variables values with your preferences (packages to install)
#                         # (The preferred way to install packages is after system installation, see Packages installation)
# ./alis.sh               # Start installation
# ./alis-reboot.sh        # (Optional) Reboot the system, only necessary when REBOOT="false"
```

## Packages installation

After the base Arch Linux system is installed, alis can install packages with pacman, Flatpak, SDKMAN and from AUR.

```bash
#                                  # After system installation start a user session
# curl -sL https://raw.githubusercontent.com/picodotdev/alis/master/download.sh | bash     # Download alis scripts
# # curl -sL https://git.io/JeaH6 | bash                                                   # Alternative download URL with URL shortener
# ./alis-packages-asciinema.sh     # (Optional) Start asciinema video recording
# vim alis-packages.conf           # Edit configuration and change variables values with your preferences (packages to install)
# ./alis-packages.sh               # Start packages installation
```

## Recovery

Boot from the latest [original Arch Linux installation media](https://www.archlinux.org/download/). After boot use the following commands to start the recovery, this will allow you to enter in the arch-chroot environment.

```bash
#                                  # Start the system with latest Arch Linux installation media
# loadkeys [keymap]                # Load keyboard keymap, eg. loadkeys es, loadkeys us, loadkeys de
# iwctl --passphrase "[WIFI_KEY]" station [WIFI_INTERFACE] connect "[WIFI_ESSID]"          # (Optional) Connect to WIFI network. _ip link show_ to know WIFI_INTERFACE.
# curl -sL https://raw.githubusercontent.com/picodotdev/alis/master/download.sh | bash     # Download alis scripts
# # curl -sL https://git.io/JeaH6 | bash                                                   # Alternative download URL with URL shortener
# ./alis-recovery-asciinema.sh     # (Optional) Start asciinema video recording
# vim alis-recovery.conf           # Edit configuration and change variables values with your last installation configuration with alis (mainly device and partition scheme)
# ./alis-recovery.sh               # Start recovery
# ./alis-recovery-reboot.sh        # Reboot the system
```

## [General Post Install Recommendations](https://www.reddit.com/r/archlinux/comments/10720sd/comment/j3kelab/?utm_source=share&utm_medium=web2x&context=3)

- Enable `paccache.timer` to clear the package cache weekly
- If using an SSD, enable `fstrim.timer` to discard unused blocks periodically
- Setup a firewall such as `ufw` or `firewalld`
- Install and configure `reflector` to frequently update the mirrorlist automatically
- Enable Parallel Downloads in `/etc/pacman.conf`
- Install `intel-ucode` or `amd-ucode` microcode depending on your CPU
- For laptops, setup CPU frequency scaling and optimise battery life with `tlp`, `autocpu-freq`, `powertop` or `power-profiles-daemon` etc
- Install a backup kernel like LTS or Zen kernel
- For NVIDIA users, create a [pacman hook](https://wiki.archlinux.org/title/NVIDIA#pacman_hook) to ensure `initramfs` gets updated on every `nvidia` or kernel upgrade
- Install noto-fonts for basic font coverage
- Optionally, replace PulseAudio with PipeWire

## [Pacman Hook](https://wiki.archlinux.org/title/NVIDIA#pacman_hook)

- `/etc/pacman.d/hooks/nvidia.hook`
- [Pacman Hooks - Multiple Targets](https://www.reddit.com/r/archlinux/comments/bp0alr/pacman_hooks_multiple_targets/?utm_source=share&utm_medium=android_app&utm_name=androidcss&utm_term=2&utm_content=share_button)
- [Is NeedsTargets in pacman mkinitcipo hooks unnecessary?](https://bbs.archlinux.org/viewtopic.php?id=263491)

```bash
[Trigger]
Operation=Install
Operation=Upgrade
Operation=Remove
Type=Package
Target=nvidia-dkms
Target=linux-lts
Target=linux-zen
# Change the linux part above and in the Exec line if a different kernel is used
[Action]
Description=Update Nvidia module in initcpio
Depends=mkinitcpio
When=PostTransaction
NeedsTargets
Exec=/bin/sh -c 'while read -r trg; do case $trg in [ linux-lts | linux-zen ] ) exit 0; esac; done; /usr/bin/mkinitcpio -P'
```
