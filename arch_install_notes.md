# [Arch Linux Installation Guide (LUKS/BTRFS/KDE)](https://www.youtube.com/watch?v=hJRfMgpke3E&list=WL&index=6)

- [Arch Linux Installation Guide (LUKS/BTRFS/KDE)](#arch-linux-installation-guide-luksbtrfskde)
  - [Connect to Wi-Fi](#connect-to-wi-fi)
  - [Preparation](#preparation)
  - [Partition the Disk](#partition-the-disk)
  - [Create amd Mount Partitions/Subvolumes](#create-amd-mount-partitionssubvolumes)
  - [create `SWAP` file](#create-swap-file)
  - [Installation](#installation)
    - [Install essential packages](#install-essential-packages)
    - [Configure the System](#configure-the-system)
    - [Install KDE Plasma](#install-kde-plasma)
  - [Post-Install Config](#post-install-config)
  - [NVIDIA Drivers](#nvidia-drivers)
    - [Video Guides](#video-guides)
    - [NVIDIA Prime (***Officially Supported***)](#nvidia-prime-officially-supported)
    - [DRM kernel mode setting](#drm-kernel-mode-setting)
      - [`pacman` Hook](#pacman-hook)
    - [Check Functionality](#check-functionality)
    - [NVIDIA Optimus](#nvidia-optimus)
      - [Usage](#usage)
        - [System Tray App](#system-tray-app)
  - [Snapshots](#snapshots)
    - [Snapper Setup](#snapper-setup)
    - [Manual Snapshots](#manual-snapshots)
      - [Simple Shapshots](#simple-shapshots)
    - [Wrapping `pacman` Transactions in Snapshots](#wrapping-pacman-transactions-in-snapshots)
    - [AppImages](#appimages)
    - [Tips/Tricks](#tipstricks)
      - [`grub-btrfs`](#grub-btrfs)
  - [`arch-chroot` into installed system](#arch-chroot-into-installed-system)
  - [Issues](#issues)
    - [Bootloader](#bootloader)
    - [Wi-Fi](#wi-fi)
    - [`grub-btrfs`](#grub-btrfs-1)
  - [TODOs](#todos)

## Connect to Wi-Fi

- `iwd` [iNet wireless daemon](https://wiki.archlinux.org/index.php/Iwd) is a wireless daemon for Linux written by Intel.
- to get an interactive prompt do: `iwctl`
- the interactive prompt is then displayed with a prefix of `[iwd]#`
- connect to a network:
  - list all Wi-Fi devices: `[iwd]# device list`
  - scan for networks: `[iwd]# station device scan`
  - list all available networks: `[iwd]# station device get-networks`
  - connect to a network: `[iwd]# station device connect SSID`
    - if a passphrase is required, you will be prompted to enter it

## Preparation

- check internet connection `ping -c 4 8.8.8.8`
  - `8.8.8.8` is [Google DNS](https://developers.google.com/speed/public-dns)
- sync network time `timedatectl set-ntp true`

## Partition the Disk

- list all disks `lsblk`
- partition the disk `cfdisk /dev/nvme0n1`
  - `256M EFI System`
  - `512M Linux filesystem`
  - `#M Linux filesystem`

## Create amd Mount Partitions/Subvolumes

- create the file system
  - `mkfs.vfat -n “EFI System” /dev/nvme0n1p1`
  - `mkfs.ext4 -L boot /dev/nvme0n1p2`
  - `mkfs.ext4 -L root /dev/nvme0n1p3`
- [encypt the disk](https://wiki.archlinux.org/index.php/Dm-crypt/Device_encryption#Encryption_options_for_LUKS_mode)
  - `modprobe dm-crypt`
  - `modprobe dm-mod`
  - `cryptsetup luksFormat -v -s 512 -h sha512 /dev/nvme0n1p3`
    - password: `some password`
- mount partitions
  - open `LUKS`
    - `cryptsetup open /dev/nvme0n1p3 archlinux`
      - enter the password
    - `ls /dev/mapper/archlinux`
  - [create/mount file system](https://wiki.archlinux.org/index.php/Btrfs#File_system_on_a_single_device)
    - `mkfs.btrfs -L root /dev/mapper/archlinux`
    - `mount -t btrfs /dev/mapper/archlinux /mnt`
    - `cd /mnt`
  - [create/mount `brtfs` subvolumes](https://wiki.archlinux.org/index.php/Btrfs#Subvolumes) - must be in `/mnt`
    - `btrfs subvolume create root`
    - `btrfs subvolume create home`
    - `btrfs subvolume create snapshots`
    - `umount -R /mnt`
    - `mount -t btrfs -o subvol=root /dev/mapper/archlinux /mnt`
    - `mkdir /mnt/home`
    - `mount -t btrfs -o subvol=home /dev/mapper/archlinux /mnt/home`
    - `mkdir /mnt/snapshots`
    - `mount -t btrfs -o subvol=snapshots /dev/mapper/archlinux /mnt/snapshots`
    - `mkdir /mnt/boot`
    - `mkdir /mnt/boot/efi` - ***the folder may not be created properly***
    - `mount /dev/nvme0n1p2 /mnt/boot`
    - `mkdir /mnt/boot/efi`
    - `mount /dev/nvme0n1p1 /mnt/boot/efi`

## create `SWAP` file

- the proper way to initialize a swap file is to ***first create a non-compressed, non-snapshotted subvolume to host the file***, *cd* into its directory, then create a zero length file, set the `No_COW` attribute on it with [chattr](https://wiki.archlinux.org/index.php/File_permissions_and_attributes#chattr_and_lsattr), and make sure compression is disabled:

    ```bash
    cd /
    btrfs subvolume create swap
    cd /swap/
    truncate -s 0 /swap/swapfile
    chattr +C /swap/swapfile
    btrfs property set /swap/swapfile compression none
    ```

  - set the right permissions (a world-readable swap file is a huge local vulnerability): `chmod 600 /swap/swapfile`
  - allocate 2GB to swapfile: `fallocate -l 2G /swap/swapfile`
  - after creating the correctly sized file, format it to swap: `mkswap /swap/swapfile`
  - activate the swap file: `swapon /swap/swapfile`
  - finally, edit the fstab configuration to add an entry for the swap file:
    - `sudo nano /etc/fstab`
      - `/swap/swapfile none swap defaults 0 0`

- **Note:**
  - the swap file must be specified by its location on the file system, not by its UUID or LABEL.
  - when using Btrfs, do not forget to add the created subvolume to the list as well, and remove the `discard,autodefrag` and compression options.

- **Note:** [Since Linux kernel 5.0, Btrfs has native swap file support with some limitations:](https://wiki.archlinux.org/index.php/Btrfs#Swap_file)
  - The swap file cannot be on a snapshotted subvolume. The proper procedure is to create a new subvolume to place the swap file in.
  - It does not support swap files on file systems that span multiple devices. [See Btrfs wiki: Does btrfs support swap files?](https://btrfs.wiki.kernel.org/index.php/FAQ#Does_btrfs_support_swap_files.3F) and [Arch forums discussion](https://bbs.archlinux.org/viewtopic.php?pid=1849371#p1849371).

## Installation

- [Installation Guide](https://wiki.archlinux.org/index.php/Installation_guide)

### Install essential packages

- `pacstrap -i /mnt base base-devel efibootmgr grub networkmanager vim linux linux-firmware nano`

### Configure the System

- generate an fstab file (use -U or -L to define by UUID or labels, respectively): `genfstab -U /mnt >> /mnt/etc/fstab`
  - check the resulting `/mnt/etc/fstab` file, and edit it in case of errors

- change root into the new system: `arch-chroot /mnt`
- set `root` password: `passwd`

- localization
  - edit`/etc/locale.gen` and uncomment `en_CA.UTF-8 UTF-8` and other needed locales
  - generate the locales by running: `locale-gen`
  - create the `locale.conf` file, and set the `LANG` variable accordingly:
    - `echo  LANG=en_CA.UTF-8 >> /etc/locale.conf`
  - if you set the keyboard layout, make the changes persistent in `vconsole.conf`:
    - `echo KEYMAP=us > /etc/vconsole.conf`

- timezone
  - set time zone: `ln -sf /usr/share/zoneinfo/America/Toronto /etc/localtime`
  - run `hwclock` to generate `/etc/adjtime`: `hwclock --systohc --utc`

- network configuration
  - create the hostname file: `echo arch > /etc/hostname`
  - add matching entries to hosts:
    - `nano /etc/hosts`

        ```bash
        /etc/hosts
        ---------------------------
        127.0.0.1    localhost arch
        ::1          localhost arch
        ```

- bootloader
  - edit `/etc/default/grub`
    - change `GRUB_CMDLINE_LINUX=””` to `GRUB_CMDLINE_LINUX=”cryptdevice=/dev/nvme0n1p3:archlinux”`
  - edit `/etc/mkinitcpio.conf`

    ```bash
    /etc/mkinitcpio.conf
    ----------------------------------------------------------------------------------------------------
    ...
    HOOKS=(base udev autodetect keyboard keymap consolefont modconf block encrypt lvm2 filesystems fsck)
    ...
    ```

  - `mkinitcpio -p linux`
  - `grub-install --boot-directory=/boot --efi-directory=/boot/efi /dev/nvme0n1p2`
  - `grub-mkconfig -o /boot/grub/grub.cfg`
  - `grub-mkconfig -o /boot/efi/EFI/arch/grub.cfg`

- enable [microcode](https://wiki.archlinux.org/title/Microcode) updates

```bash
sudo pacman -S intel-ucode
sudo grub-mkconfig -o /boot/grub/grub.cfg
reboot

# confirm installation after reboot
➜  ~ sudo dmesg | grep -i microcode                      
[    0.000000] microcode: microcode updated early to revision 0xea, date = 2021-01-05
[    0.165690] SRBDS: Mitigation: Microcode
[    0.716554] microcode: sig=0x906ea, pf=0x20, revision=0xea
[    0.716834] microcode: Microcode Update Driver: v2.2.
```

- reboot
  - `exit`
  - `reboot`
  - unlock encytpted drive: password
  - login as root user
    - username: `root`
    - password: `password`

- add new user
  - `useradd --create-home --home /home/ystanev ystanev`
  - set password for new user: `passwd ystanev`
  - add user to `wheel` group: `usermod -G wheel ystanev`

- sudo
  - to establish **nano** as the visudo editor for the duration of the current shell session, `export EDITOR=nano`
  - `visudo`
    - uncomment the following line: `%wheel ALL=(ALL) NOPASSWD: ALL`
    - save/close the file: `Ctrl+S`, `Ctrl+X`

- enable/start `NetworkManager` service and check connectivity:
  - `systemctl enable NetworkManager --now`
  - `ping -c 4 8.8.8.8`

- login as newly created user
  - logout from root user `exit`
  - login as new user
    - username: `ystanev`
    - password: `password`
  - check `sudo`: `sudo ls`

### Install KDE Plasma

- [KDE Plasma](https://wiki.archlinux.org/index.php/KDE)
- install [Plasma](https://archlinux.org/groups/x86_64/plasma/) group, required application from [KDE-Applications](https://archlinux.org/groups/x86_64/kde-applications/) group and [SDDM](https://wiki.archlinux.org/index.php/SDDM) display manager `pacman -S plasma sddm ark dolphin dolphin-plugins filelight gwenview kate kcalc kompare konsole okular spectacle partitionmanager`
- enable/start SDDM `sudo systemctl enable sddm --now`
- `reboot`

## Post-Install Config

- `AUR` setup
  - open `konsole`
    - `sudo pacman -S --needed base-devel git wget curl`
    - `cd /tmp`
    - `git clone https://aur.archlinux.org/package-query.git; cd ./package-query; makepkg -si`
    - `cd ../; git clone https://aur.archlinux.org/yay.git; cd yay; makepkg -si`

- setup additional mirrors
  - `sudo nano /etc/pacman.conf`
    - uncomment the following lines

      ```bash
      [multilib]
      Include = etc/pacman.d/mirrorlist
      ```

    - `sudo pacman -Syu`

- [additional steps for `VirtualBox`](https://wiki.archlinux.org/index.php/VirtualBox/Install_Arch_Linux_as_a_guest)
  - `sudo pacman -S install virtualbox-guest-utils`
  - `sudo systemctl enable vboxservice.service --now`
  - `pacman -Syu`

## NVIDIA Drivers

### Video Guides

- [NVIDIA Optimus Setup - Arch, Manjaro, EndeavourOS](https://www.youtube.com/watch?v=OlIXQRpfJQ4)
- [Arch Linux: NVIDIA Optimus](https://www.youtube.com/watch?v=jncc3QL8RWI)

### NVIDIA Prime (***Officially Supported***)

- [install closed source drivers](https://wiki.archlinux.org/index.php/NVIDIA#Installation)
  - current kernel/driver: `sudo pacman -S nvidia nvidia-utils nvidia-settings`
  - lts kernel/driver: `sudo pacman -S nvidia-lts nvidia-utils nvidia-settings`
- [PRIME render offload](https://wiki.archlinux.org/index.php/PRIME#PRIME_render_offload)
  - `sudo pacman -S nvidia-prime`
  - to run a program on the NVIDIA card you can use the `prime-run`

### DRM kernel mode setting

- to enable this feature, add the `nvidia-drm.modeset=1` kernel parameter for basic functionality that should suffice
  - To make the change persistent after reboot, you could manually edit `/boot/grub/grub.cfg` with the exact line from above, or if using `grub-mkconfig`:
edit `/etc/default/grub` and append your kernel options between the quotes in the `GRUB_CMDLINE_LINUX_DEFAULT` line:
    - `GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet nvidia-drm.modeset=1"`
  - And then automatically re-generate the *grub.cfg* file with: `grub-mkconfig -o /boot/grub/grub.cfg`
- if you want to ensure it's loaded at the earliest possible occasion, or are noticing startup issues (such as the `nvidia` kernel module being loaded after the display manager) you can add `nvidia`, `nvidia_modeset`, `nvidia_uvm` and `nvidia_drm` to the initramfs according to [Mkinitcpio#MODULES](https://wiki.archlinux.org/title/Mkinitcpio#MODULES)
  - The primary configuration file for *mkinitcpio* is `/etc/mkinitcpio.conf`
  - `MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)`
- if added to the initramfs do not forget to run `mkinitcpio` every time there is a `nvidia` driver update

#### `pacman` Hook

- to avoid the possibility of forgetting to update initramfs after an NVIDIA driver upgrade, you may want to use a pacman hook:

```conf
/etc/pacman.d/hooks/nvidia.hook
-----------------------------------

[Trigger]
Operation=Install
Operation=Upgrade
Operation=Remove
Type=Package
Target=nvidia-lts
Target=linux-lts
# Change the linux part above and in the Exec line if a different kernel is used

[Action]
Description=Update Nvidia module in initcpio
Depends=mkinitcpio
When=PostTransaction
NeedsTargets
Exec=/bin/sh -c 'while read -r trg; do case $trg in linux-lts) exit 0; esac; done; /usr/bin/mkinitcpio -P'
```

- Make sure the Target package set in this hook is the one you have installed in steps above (e.g. `nvidia`, `nvidia-dkms`, `nvidia-lts` or `nvidia-ck-something`)

### Check Functionality

```bash
prime-run glxinfo | grep "OpenGL renderer"
prime-run vulkaninfos
```

### NVIDIA Optimus

- [optimus-manager](https://github.com/Askannz/optimus-manager)
- you ***must*** have the proprietary nvidia driver installed on your system
- you can install `optimus-manager` from this AUR package : [optimus-manager](https://aur.archlinux.org/packages/optimus-manager/)
  - `yay -S optimus-manager`

#### Usage

Run

- `optimus-manager --switch nvidia` to switch to the Nvidia GPU
- `optimus-manager --switch integrated` to switch to the integrated GPU and power the Nvidia GPU off
- `optimus-manager --switch hybrid` to switch to the iGPU but leave the Nvidia GPU available for on-demand offloading, similar to how Optimus works on Windows
- **WARNING** : Switching mode automatically logs you out, so make sure you save your work and close all your applications before doing so

##### System Tray App

![tray app](https://github.com/Askannz/optimus-manager/raw/master/systray.png)

- the program [optimus-manager-qt](https://github.com/Shatur95/optimus-manager-qt) provides a system tray icon for easy switching
- it also includes a GUI for setting options without editing the configuration file manually
- AUR Package: [optimus-manager-qt](https://aur.archlinux.org/packages/optimus-manager-qt/)
  - `yay -S optimus-manager-qt`

## Snapshots

- [Snapper Arch Wiki](https://wiki.archlinux.org/index.php/snapper)

### Snapper Setup

- install/activate `cronie` for automatic snapshots
  - `sudo pacman -S cronie`
  - `sudo systemctl enable cronie.service --now`

- installsation
  - `sudo pacman -S snapper`
  - `sudo pacman -S grub-btrfs`
    - `grub-mkconfig -o /boot/grub/grub.cfg`
    - [generate your Grub menu after installation for the changes to take effect](https://github.com/Antynea/grub-btrfs): `grub-mkconfig -o /boot/grub/grub.cfg`
    - if you would like grub-btrfs menu to automatically update when a snapshot is created or deleted: `systemctl enable grub-btrfs.path`
  - `yay -S snapper-gui-git`

- create config
  - `sudo snapper -c root create-config /`
  - `sudo snapper -c home create-config /home`

- [set snapshots limits](https://wiki.archlinux.org/index.php/snapper#Set_snapshot_limits)

```bash
/etc/snapper/configs/config
---------------------------------------------
TIMELINE_MIN_AGE="1800"
TIMELINE_LIMIT_HOURLY="8"
TIMELINE_LIMIT_DAILY="3"
TIMELINE_LIMIT_WEEKLY="2"
TIMELINE_LIMIT_MONTHLY="1"
TIMELINE_LIMIT_YEARLY="0"
```

### Manual Snapshots

#### Simple Shapshots

- by default snapper takes snapshots that are of the simple type, having no special relationship to other snapshots

```bash
sudo snapper -c root create --description "Initial snapper setup"
sudo snapper -c home create --description "Initial snapper setup"
```

- the above command does not use any cleanup algorithm, so the snapshot is stored permanently or until deleted

### Wrapping `pacman` Transactions in Snapshots

- `snap-pac` - cacman hooks that use snapper to create pre/post btrfs snapshots like openSUSE's YaST
- `snap-pac-grub` - pacman hook to update GRUB entries for grub-btrfs after snap-pac made snapshots

```bash
sudo pacman -S snap-pac
gpg --recv-keys EB4F9E5A60D32232BB52150C12C87A28FEAC6B20
yay -S snap-pac-grub
```

### AppImages

- to run `*.appimage` [you need to install `fuse`](https://docs.appimage.org/user-guide/troubleshooting/fuse.html): `pacman -S fuse`

### Tips/Tricks

- [wrapping pacman transactions in snapshots](https://wiki.archlinux.org/index.php/snapper#Wrapping_pacman_transactions_in_snapshots)
- [incremental backup to external drive](https://wiki.archlinux.org/index.php/snapper#Incremental_backup_to_external_drive)
- Magic SysRq key - [REISUB](https://wiki.archlinux.org/index.php/Keyboard_shortcuts#Kernel_(SysRq))
- [Zswap](https://wiki.archlinux.org/index.php/zswap) is a kernel feature that provides a compressed RAM cache for swap pages
  - in the stable [linux](https://archlinux.org/packages/?name=linux) official kernel, **zswap is enabled by default**.

#### `grub-btrfs`

- [grub-btrfs](https://archlinux.org/packages/community/any/grub-btrfs/)
- improves Grub by adding "btrfs snapshots" to the Grub menu
- you can boot your system on a "snapshot" from the Grub menu
- **[Warning: booting on read-only snapshots can be tricky](https://github.com/Antynea/grub-btrfs)**
- if you choose to do it, `/var/log` (prefered) or even `/var` must be on a separate subvolume.

```bash
btrfs subvolume create log
mkdir -p /mnt/var/log
mount -t btrfs -o subvol=log /dev/mapper/archlinux /mnt/var/log
```

```bash
btrfs subvolume create var
mkdir /mnt/var
mount -t btrfs -o subvol=var /dev/mapper/archlinux /mnt/var
```

- otherwise, make sure your snapshots are writeable, see [this ticket](https://github.com/Antynea/grub-btrfs/issues/92) for more info
- this project includes its own solution, refer to the [documentation](https://github.com/Antynea/grub-btrfs/blob/master/initramfs/readme.md)
  1. `pacman -S grub-btrfs`
  2. edit the file `/etc/mkinitcpio.conf` add hook `grub-btrfs-overlayfs` at the end of the line `HOOKS`
     - e.g. - `HOOKS=(base udev autodetect modconf block filesystems keyboard fsck grub-btrfs-overlayfs)`
  3. re-generate your initramfs `mkinitcpio -P` (option `-P` means, all preset present in `/etc/mkinitcpio.d`)

## `arch-chroot` into installed system

- [changing root](https://wiki.archlinux.org/index.php/chroot) is commonly done for performing system maintenance on systems where booting and/or logging in is no longer possible

```bash
cryptsetup open --type luks /dev/nvme0n1p3 archlinux
mount -t btrfs -o subvol=root /dev/mapper/archlinux /mnt
mount -t btrfs -o subvol=home /dev/mapper/archlinux /mnt/home
mount -t btrfs -o subvol=snapshots /dev/mapper/archlinux /mnt/snapshots
mount /dev/nvme0n1p2 /mnt/boot # if the system has separate /boot partition (rare case)
mount /dev/nvme0n1p1 /mnt/boot/efi # if the system boots in UEFI mode
lsblk -f # to check the mounf points
arch-chroot /mnt
```

## Issues

### Bootloader

- messed up `grub` setup
- copied `/boot/efi/*.img` to `/boot/*.img`
- re-ran `grub` [install/setup](#configure-the-system)

### Wi-Fi

- [no Wi-Fi post install](https://bbs.archlinux.org/viewtopic.php?id=264224)
  - `wlan0` can see the networks, but can't connect
    - `no wlan0 device found`
  - [check drivers](https://wiki.archlinux.org/index.php/Network_configuration/Wireless#Check_the_driver_status)
  - [check `NetworkManager`](https://wiki.archlinux.org/index.php/NetworkManager)
  - **PROBLEM**: conflict between `NetworkManager` and `iwd`
  - ***SOLUTION***: stop/disable `iwd`
  
### `grub-btrfs`

- snapshots don't show up in the `Grub` menu
- solved after the latest update breakage on 12.03.2021 - remove/regenarate `/etc/fstab`

## TODOs

- resolve minor sceentearing/vsync on external monitor
  - https://www.makeuseof.com/fix-screen-tearing-on-linux/
  - https://wiki.archlinux.org/title/NVIDIA/Troubleshooting#Avoid_screen_tearing
  - https://gitlab.com/btrfs-assistant/btrfs-assistant
